package com.virjar.ucrack.hermes.weishi;

import com.virjar.hermes.hermesagent.hermes_api.ActionRequestHandler;
import com.virjar.hermes.hermesagent.hermes_api.WrapperAction;
import com.virjar.hermes.hermesagent.hermes_api.aidl.InvokeRequest;
import com.virjar.hermes.hermesagent.hermes_api.aidl.InvokeResult;
import com.virjar.xposed_extention.ClassLoadMonitor;

import org.apache.commons.lang3.StringUtils;

import de.robv.android.xposed.XposedHelpers;

@WrapperAction("GetPersonalPage")
public class GetPersonalPageHandler implements ActionRequestHandler {
    @Override
    public Object handleRequest(InvokeRequest invokeRequest) {
        Class beanClass = ClassLoadMonitor.tryLoadClass("com.tencent.oscar.module.f.b.a.b");
        if (beanClass == null) {
            //可能由于热加载原因，导致这个class并不存在，此时路由到另一个存在的接口中去
            return WSGetPersonalPage.instance.handleRequest(invokeRequest);
        }
        String userID = invokeRequest.getString("userID");
        if (StringUtils.isBlank(userID)) {
            return InvokeResult.failed("the param {userID} not presented");
        }
        String attach_info = invokeRequest.getString("attach_info");
        return WeishiUtil.sendRequest(XposedHelpers.newInstance(beanClass
                , userID, 0, attach_info));
    }
}
